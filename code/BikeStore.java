//From Oscar Palencia
public class BikeStore {
    public static void main(String[] args) {
       Bicycle[] bicycles = new Bicycle[4];
       bicycles[0] = new Bicycle("3T Bikes", 5, 30.0);
       bicycles[1] = new Bicycle("Alchemy Bicycles", 7, 32.0);
       bicycles[2] = new Bicycle("Ancheer Bikes", 3, 25.0);
       bicycles[3] = new Bicycle("Bianchi Bicycles", 6, 30.0);

       for (Bicycle bicycle : bicycles) {
           System.out.println(bicycle.toString());
       }
    }
}
